# Dialyzer

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `dialyzer` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:dialyzer, "~> 0.1.0"}]
    end
    ```

  2. Ensure `dialyzer` is started before your application:

    ```elixir
    def application do
      [applications: [:dialyzer]]
    end
    ```

