defmodule Dialyzer do
    @spec test() :: list
    def test(), do: []

    @spec test1(number) :: list
    def test1(number), do: test

    @spec test2(number) :: list
    def test2(number), do: test1(1)
end
